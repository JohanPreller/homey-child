<?php
if(!function_exists('homey_add_earning')) {
    function homey_add_earning($reservation_id) {
        global $wpdb;
        $allowded_html = array();

        $reservation_payment = homey_option('reservation_payment');
        if($reservation_payment != 'percent' && $reservation_payment != 'full') {
            return;
        }
       

        //host fee in % set by admin
        $host_fee_percent = homey_get_host_fee_percent();

        if(empty($reservation_id)) {
            return;
        }
        $reservation_meta = get_post_meta($reservation_id, 'reservation_meta', true);
        $extra_options = get_post_meta($reservation_id, 'extra_options', true);

        $listing_host = get_post_meta($reservation_id, 'listing_owner', true);
        $listing_guest = get_post_meta($reservation_id, 'listing_renter', true);
        $is_hourly = get_post_meta($reservation_id, 'is_hourly', true);

        $listing_id     = intval($reservation_meta['listing_id']);
        $check_in_date  = wp_kses ( $reservation_meta['check_in_date'], $allowded_html );
        $check_out_date = '';
        $guests         = intval($reservation_meta['guests']);

        $booking_type = homey_booking_type_by_id($listing_id);
        
        if($is_hourly != 'yes') {
            $check_out_date = wp_kses ( @$reservation_meta['check_out_date'], $allowded_html );

            if( $booking_type == 'per_week' ) {
                $prices_array = homey_get_weekly_prices($check_in_date, $check_out_date, $listing_id, $guests, $extra_options);
            } else if( $booking_type == 'per_month' ) {
                $prices_array = homey_get_monthly_prices($check_in_date, $check_out_date, $listing_id, $guests, $extra_options);
            } else if( $booking_type == 'per_day_date' ) {
                $prices_array = homey_get_day_date_prices($check_in_date, $check_out_date, $listing_id, $guests, $extra_options);
            } else {
                $prices_array = homey_get_prices($check_in_date, $check_out_date, $listing_id, $guests, $extra_options);
            }

        } else {
            $check_in_hour = get_post_meta($reservation_id, 'reservation_checkin_hour', true);
            $check_out_hour = get_post_meta($reservation_id, 'reservation_checkout_hour', true);

            $prices_array = homey_get_hourly_prices($check_in_hour, $check_out_hour, $listing_id, $guests, $extra_options);
        }
        
        
        // $total_price = $prices_array['total_price'];
        $total_price = ($prices_array['total_price'] != "") ? $prices_array['total_price'] : 0;
        // $services_fee = $prices_array['services_fee'];
        $services_fee = ($prices_array['services_fee'] != "") ? $prices_array['services_fee'] : 0;
        // $security_deposit = $prices_array['security_deposit'];
        $security_deposit = ($prices_array['security_deposit'] != "") ? $prices_array['security_deposit'] : 0;
        // $upfront_payment = $prices_array['upfront_payment'];
        $upfront_payment = ($prices_array['upfront_payment'] != "") ? $prices_array['upfront_payment'] : 0;
        // $payment_due = $prices_array['balance'];
        $payment_due = ($prices_array['balance'] != "") ? $prices_array['balance'] : 0;

        $extra_expenses = homey_get_extra_expenses($reservation_id);
        $extra_discount = homey_get_extra_discount($reservation_id);

        if(!empty($extra_expenses)) {
            $expenses_total_price = $extra_expenses['expenses_total_price'];
            $total_price = $total_price + $expenses_total_price;
        }

        if(!empty($extra_discount)) {
            $discount_total_price = $extra_discount['discount_total_price'];
            $total_price = $total_price - $discount_total_price;
        }

        if(homey_option('reservation_payment') == 'full') {
            $upfront_payment = $total_price; 
        }


        //deduct services fee and security deposit from total
		brave_write_log("services_fee: " . $services_fee . " & security_deposit: " . $security_deposit);
        $sf_and_sd = $services_fee + $security_deposit;

        //chargeable amount for host fee
        $chargeable_amount = $total_price - $sf_and_sd;

        //Host fee
        $host_fee = ($host_fee_percent / 100) * $chargeable_amount;

        /*
        * Calculate net earning.
        * Net earning will be on upfront payment
        */
        $net_earnings = $upfront_payment - $services_fee; //deduct services fee from $upfront
        $net_earnings = $net_earnings - $host_fee; //deduct host fee from upfront_payment
        $net_earnings = $net_earnings - $security_deposit; //deduct security deposit

        $table_name = $wpdb->prefix . 'homey_earnings';

        $is_added = $wpdb->query(
            "SELECT * FROM $table_name WHERE user_id = $listing_host AND  guest_id = $listing_guest AND listing_id = $listing_id AND reservation_id = $reservation_id"
        );

        if($is_added < 1){
            $order_id = $wpdb->query( $wpdb->prepare(
                "INSERT INTO $table_name
            ( user_id, guest_id, listing_id, reservation_id, services_fee, host_fee, upfront_payment, payment_due, net_earnings, total_amount, security_deposit, chargeable_amount, host_fee_percent )
            VALUES ( %d, %d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s )",
                $listing_host,
                $listing_guest,
                $listing_id,
                $reservation_id,
                $services_fee,
                $host_fee,
                $upfront_payment,
                $payment_due,
                $net_earnings,
                $total_price,
                $security_deposit,
                $chargeable_amount,
                $host_fee_percent
            ) );

            if($order_id) {
                $all_fees = $services_fee + $security_deposit + $host_fee;
                $total_net_earnings = $total_price - $all_fees;
                homey_add_host_earnings($listing_host, $net_earnings, $total_net_earnings);
                homey_add_guest_security($listing_guest, $security_deposit);
            }
        }
    }
}
?>