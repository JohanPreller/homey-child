<?php
if ( !function_exists( 'homey_start_thread' ) ) {

	function homey_start_thread() {
		brave_write_log("homey_start_thread1");
		brave_write_log(print_r($_POST, true));

		$messages_page = homey_get_template_link_2('template/dashboard-messages.php');

		$nonce = $_POST['start_thread_form_ajax'];

		if ( !wp_verify_nonce( $nonce, 'start-thread-form-nonce') ) {
			echo json_encode( array(
				'success' => false,
				'msg' => esc_html__('Unverified Nonce!', 'homey')
			));
			wp_die();
		}

		if ( empty( $_POST['message'] ) ) {
			echo json_encode( array(
				'success' => false,
				'msg' => esc_html__('Please write something in message', 'homey')
			));
			wp_die();
		}

		if ( isset( $_POST['listing_id'] ) && !empty( $_POST['listing_id'] ) && isset( $_POST['message'] ) && !empty( $_POST['message'] ) ) {

			$message_attachments = array();
			if ( isset( $_POST['listing_image_ids'] ) && sizeof( $_POST['listing_image_ids'] ) != 0 ) {
				$message_attachments = $_POST['listing_image_ids'];
			}
			$message_attachments = serialize( $message_attachments );

			$message = sanitize_textarea_field($_POST['message']);
			$thread_id = apply_filters( 'homey_start_thread', $_POST );
			$message_id = apply_filters( 'homey_thread_message', $thread_id, $message, $message_attachments );

			if ( $message_id ) {

				$redirect_link = add_query_arg( array(
		            'thread_id' => $thread_id
		        ), $messages_page );

				echo json_encode(
					array(
						'success' => true,
						'redirect_link' => $redirect_link,
						'msg' => esc_html__("Message sent successfully!", 'homey')
					)
				);

				wp_die();

			}

		}

		echo json_encode(
			array(
				'success' => false,
				'msg' => esc_html__("Some errors occurred! Please try again.", 'homey')
			)
		);

		wp_die();

	}

}

if ( !function_exists( 'homey_thread_message' ) ) {

	function homey_thread_message() {
		$user_id = apply_filters( 'determine_current_user', false );
		wp_set_current_user( $user_id );
		$current_user = wp_get_current_user();
		$userID       = $current_user->ID;
		brave_write_log("homey_thread_message1: userID: " . $userID);

		$nonce = $_POST['start_thread_message_form_ajax'];

		if ( !wp_verify_nonce( $nonce, 'start-thread-message-form-nonce') ) {
			echo json_encode( array(
				'success' => false,
				'url' => homey_get_template_link_2('template/dashboard-messages.php') . '?' . http_build_query( array( 'thread_id' => $thread_id, 'success' => false ) ),
				'msg' => esc_html__('Unverified Nonce!', 'homey')
			));
			wp_die();
		}

		if ( empty( $_POST['message'] ) ) {
			echo json_encode( array(
				'success' => false,
				'msg' => esc_html__('Please write something in message', 'homey')
			));
			wp_die();
		}

		if ( isset( $_POST['thread_id'] ) && !empty( $_POST['thread_id'] ) && isset( $_POST['message'] ) && !empty( $_POST['message'] ) ) {
			$message_attachments = Array ();
			$thread_id = intval($_POST['thread_id']);
			$message = sanitize_textarea_field($_POST['message']);

			if ( isset( $_POST['listing_image_ids'] ) && sizeof( $_POST['listing_image_ids'] ) != 0 ) {
				$message_attachments = $_POST['listing_image_ids'];
			}
			$message_attachments = serialize( $message_attachments );

            $user_id = apply_filters( 'determine_current_user', false );
            wp_set_current_user( $user_id );
            $current_user = wp_get_current_user();
            $userID       = $current_user->ID;
			brave_write_log("homey_thread_message2: userID: " . $userID);

			$message_id = apply_filters( 'homey_thread_message', $thread_id, $message, $message_attachments );

			if ( $message_id ) {

				echo json_encode(
					array(
						'success' => true,
						'url' => homey_get_template_link_2('template/dashboard-messages.php') . '?' . http_build_query( array( 'thread_id' => $thread_id, 'success' => true ) ),
						'msg' => esc_html__("Thread success fully created!", 'homey')
					)
				);

				wp_die();

			}

		}

		echo json_encode(
			array(
				'success' => false,
				'url' => homey_get_template_link_2('template/dashboard-messages.php') . '?' . http_build_query( array( 'thread_id' => $thread_id, 'success' => false ) ),
				'msg' => esc_html__("Some errors occurred! Please try again.", 'homey')
			)
		);

		wp_die();

	}

}

if(!function_exists('save_homey_create_messages_thread')) {
	function save_homey_create_messages_thread($guest_message, $reservation_id, $userID = '' ) {
		brave_write_log("save_homey_create_messages_thread: guest_message: " . $guest_message . " & reservation_id: " . $reservation_id . " & userID: " . $userID);
		
		if ( !empty( $reservation_id ) && !empty( $guest_message ) ) {

			$message_attachments = array();

			$receiver_id = get_post_meta($reservation_id, 'listing_owner', true);

			$data = array(
				'listing_id' => $reservation_id,
				'receiver_id' => $receiver_id,
				'user_id' => $userID,
			);

			$message = $guest_message;
			$thread_id = apply_filters( 'homey_start_thread', $data );
			if ($thread_id != 0) {
				$message_id = apply_filters( 'homey_thread_message', $thread_id, $message, $message_attachments );
			}

			if ( $message_id ) {


			}

		}

	}
}

if ( !function_exists( 'homey_start_thread_filter' ) ) {

	function homey_start_thread_filter( $data ) {

		global $wpdb, $current_user;

		$my_current_user = wp_get_current_user();
		$sender_id =  intval($my_current_user->ID);
		$user_id = intval($data['user_id']);
		$listing_id = intval($data['listing_id']);
		$receiver_id = intval($data['receiver_id']);
		$table_name = $wpdb->prefix . 'homey_threads';

		brave_write_log("homey_start_thread_filter1: sender_id: " . $sender_id . " & user_id: " . $user_id);

		if( !empty($user_id) ) {
			$sender_id = $user_id;
		}

		if ($sender_id == 0) {
			return 0;
		}

		brave_write_log("homey_start_thread_filter2: sender_id: " . $sender_id . " & user_id: " . $user_id);

		$id = $wpdb->insert(
			$table_name,
			array(
				'sender_id' => $sender_id,
				'receiver_id' => $receiver_id,
				'listing_id' => $listing_id,
				'time'	=> current_time( 'mysql' )
			),
			array(
				'%d',
				'%d',
				'%d',
				'%s'
			)
		);

		return $wpdb->insert_id;

	}

}

// add_filter( 'homey_thread_message', 'homey_thread_message_filter', 3, 9 );
if ( !function_exists( 'homey_thread_message_filter' ) ) {

	function homey_thread_message_filter( $thread_id, $message, $attachments, $user_id = 0 ) {

		global $wpdb, $current_user;

		if ( is_array( $attachments ) ) {
			$attachments = serialize( $attachments );
		}

		$my_current_user = wp_get_current_user();
		$created_by =  $my_current_user->ID;
		brave_write_log("homey_thread_message_filter: created_by1: " . $created_by . " & thread_id:" . $thread_id);

		$table_name = $wpdb->prefix . 'homey_thread_messages';

		if ($created_by == 0) {
			$created_by = $user_id;
		}
		brave_write_log("homey_thread_message_filter: created_by2: " . $created_by . " & thread_id:" . $thread_id);

		if ($created_by == 0) {
			$message_query = $wpdb->prepare( "SELECT sender_id FROM " . $wpdb->prefix . "homey_threads WHERE id = %d", $thread_id );
			brave_write_log("homey_thread_message_filter: SQL: SELECT sender_id FROM " . $wpdb->prefix . "homey_threads WHERE id = " . $thread_id);
			$homey_thread = $wpdb->get_row( $message_query );
			$created_by  = $homey_thread->sender_id;
		}
		brave_write_log("homey_thread_message_filter: created_by3: " . $created_by . " & thread_id:" . $thread_id);

		$message = stripslashes($message);
		$message = htmlentities($message);

		$message_id = $wpdb->insert(
			$table_name,
			array(
				'created_by' => $created_by,
				'thread_id' => intval($thread_id),
				'message' => sanitize_textarea_field($message),
				'attachments' => $attachments,
				'time' => current_time( 'mysql' )
			),
			array(
				'%d',
				'%d',
				'%s',
				'%s',
				'%s'
			)
		);

		$tabel = $wpdb->prefix . 'homey_threads';
		$wpdb->update(
			$tabel,
			array(  'seen' => 0 ),
			array( 'id' => $thread_id ),
			array( '%d' ),
			array( '%d' )
		);

		$message_query = $wpdb->prepare( 
            "
            SELECT * 
            FROM $tabel 
            WHERE id = %d
            ", 
            $thread_id
        );

		$homey_thread = $wpdb->get_row( $message_query );
        $receiver_id  = $homey_thread->receiver_id;
		$sender_id    = $homey_thread->sender_id;

		if($created_by != $receiver_id){
			$receiver_data = get_user_by( 'id', $receiver_id );
			apply_filters( 'homey_message_email_notification', $thread_id, $message, $receiver_data->user_email, $created_by );
			brave_write_log("homey_thread_message_filter: homey_message_email_notification1: created_by: " . $created_by . " &receiver_id: " . $receiver_id);
		}
		
		if($created_by != $sender_id){
			$sender_data = get_user_by( 'id', $sender_id );
			if ($created_by != 0) {
				apply_filters( 'homey_message_email_notification', $thread_id, $message, $sender_data->user_email, $created_by );
				brave_write_log("homey_thread_message_filter: homey_message_email_notification2a: created_by: " . $created_by . " &sender_id: " . $sender_id);
			}
			brave_write_log("homey_thread_message_filter: homey_message_email_notification2b: created_by: " . $created_by . " &sender_id: " . $sender_id);
		}
		
		return $message_id;

	}

}

if ( !function_exists( 'homey_message_email_notification_filter' ) ) {

	function homey_message_email_notification_filter( $thread_id, $message, $email, $created_by ) {

		ob_start();

		$url_query = array( 'thread_id' => $thread_id, 'seen' => true );
		$thread_link = homey_get_template_link_2('template/dashboard-messages.php');
		$thread_link = add_query_arg( $url_query, $thread_link );
		$sender_name = get_the_author_meta( 'display_name', $created_by );

		?>
		<table style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;width:100%;margin:0;padding:0">
			<tbody>
			<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
				<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
					<p style="margin:0 0 15px;padding:0"><?php esc_html_e( 'You have a new message on', 'homey' ); ?> <b><?php echo esc_attr( get_option('blogname') );?></b> <?php echo esc_html_e('from', 'homey');?> <i><?php echo esc_attr($sender_name); ?></i></p>
					<div style="padding-left:20px;margin:0;border-left:2px solid #ccc;color:#888">
						<p><?php echo ''.$message; ?></p>
					</div>
					<p style="padding:20px 0 0 0;margin:0">
						<a style="color:#15bcaf" href="<?php echo esc_url( $thread_link ); ?>">
							<?php echo esc_html__('Click here to see message on website dashboard.', 'homey');?>
						</a>
					</p>
				</td>
			</tr>
			</tbody>
		</table>

		<?php
		$data = ob_get_contents();

		ob_clean();

		$subject = esc_html__( 'You have a new message!', 'homey' );

		homey_send_emails( $email, $subject, $data );

	}

}