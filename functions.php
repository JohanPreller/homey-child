<?php
require_once( get_theme_file_path() . '/framework/functions/reservation.php' );
require_once( get_theme_file_path() . '/framework/functions/wallet.php' );
require_once( get_theme_file_path() . '/framework/functions/messages.php' );

// Via https://stackoverflow.com/questions/51575669/only-allow-to-purchase-one-item-in-woocommerce-3
// Allowing adding only one unique item to cart and displaying an error message
add_filter( 'woocommerce_add_to_cart_validation', 'add_to_cart_validation', 10, 1 );
function add_to_cart_validation( $passed ) {
    if( ! WC()->cart->is_empty() ){
        wc_add_notice( __("You can add only one item to cart", "woocommerce" ), 'error' );
        $passed = false;
    }
    return $passed;
}

// Avoiding checkout when there is more than one item and displaying an error message
add_action( 'woocommerce_check_cart_items', 'check_cart_items' ); // Cart and Checkout
function check_cart_items() {
    if( sizeof( WC()->cart->get_cart() ) > 1 ){
        // Display an error message
        wc_add_notice( __("More than one items in cart is not allowed to checkout", "woocommece"), 'error' );
    }
}

add_action( 'wp_enqueue_scripts', 'homey_child_enqueue_styles' );
function homey_child_enqueue_styles() {
    wp_enqueue_style( 'style', get_stylesheet_uri(),
        array( 'parenthandle' ), 
        wp_get_theme()->get('Version')
    );
}

add_action('wp_head', 'show_template_with_path', 10);
function show_template_with_path() {
    global $template;
    if($_SERVER['HTTP_HOST'] == "localhost") {
        $page_id = get_the_ID();
        $author_id = get_post_field( 'post_author', $page_id );
        $author_name = get_the_author_meta( 'display_name', $author_id );
        $payfast_merchant_id = get_user_meta( $author_id, 'homey_payfast_merchant_id', true );
        $current_user_id = get_current_user_id();
        // echo "page_id: " . $page_id . "<br>";
        // echo "author_id: " . $author_id . " (" . $author_name . ")<br>";
        // echo "payfast_merchant_id: " . $payfast_merchant_id . "<br>";
        // echo "current_user_id: " . $current_user_id . "<br>";
        // echo "current template: " . get_template_directory_uri() . "/" . basename($template) . "<br>";
    }
}

add_action('admin_enqueue_scripts', 'homey_child_extra_styles', 100);
add_action('login_enqueue_scripts', 'homey_child_extra_styles', 100);
function homey_child_extra_styles() {
    wp_enqueue_style( 'homey-child-extra-styles', get_stylesheet_directory_uri() . '/styles/admin.css', array(), '1.0.0' );
}

add_action('wp_enqueue_scripts', 'homey_child_profile_script_fix', 100);
function homey_child_profile_script_fix()
{
    if (is_page_template('template/dashboard-profile.php') || homey_is_dashboard()) {
        global $paged, $post, $current_user;
        wp_get_current_user();
        $userID = $current_user->ID;
        $profile_data = array(
            'ajaxURL' => admin_url('admin-ajax.php'),
            'user_id' => $userID,
            'homey_upload_nonce' => wp_create_nonce('homey_upload_nonce'),
            'verify_file_type' => esc_html__('Valid file formats', 'homey'),
            'homey_site_url' => esc_url( home_url() ),
            'process_loader_refresh' => 'fa fa-spin fa-refresh',
            'process_loader_spinner' => 'fa fa-spin fa-spinner',
            'process_loader_circle' => 'fa fa-spin fa-circle-o-notch',
            'process_loader_cog' => 'fa fa-spin fa-cog',
            'success_icon' => 'fa fa-check',
            'processing_text' => esc_html__('Processing, Please wait...', 'homey'),
            'gdpr_agree_text' => esc_html__('Please Agree with GDPR', 'homey'),
            'sending_info' => esc_html__('Sending info', 'homey'),
        );
        wp_dequeue_script('homey-profile');
        wp_enqueue_script('homey-child-profile', get_stylesheet_directory_uri().'/scripts/homey-child-profile.js', array('jquery'));
        wp_localize_script('homey-child-profile', 'homeyProfile2', $profile_data);
    }
}

add_action( 'woocommerce_receipt_payfast', 'add_split_payment_info_to_payfast_form', 1 );
function add_split_payment_info_to_payfast_form() {
    $split_data = addSplitPaymentParametersToPayfast();
    // echo "<br>split_data: " . $split_data . "<br>";
    if ($split_data) {
        // $split_field = "<input type='hidden' name='setup' value=" . $split_data . ">";
        // echo "<br>Check the console ...";
        echo "<script>";
        echo "jQuery(function() {";
        echo "jQuery('#submit_payfast_payment_form').on('click', function(e) {";
        echo "e.preventDefault();";
        // echo "jQuery('<input>').attr({
        //         type: 'hidden',
        //         id: 'setup',
        //         name: 'setup',
        //         value: \"" . $split_data . "\"
        //     }).appendTo('#payfast_payment_form');";
        echo 'jQuery("#payfast_payment_form").append("<input type=\'hidden\' name=\'setup\' value=\'' . htmlspecialchars($split_data) . '\'>");';
        echo "jQuery('#payfast_payment_form').submit();";
        // echo "console.log('split_data: " . $split_data . "');";
        echo "});";
        echo "});";
        echo "</script>";
    }
    else {
        // echo "No split data";
    }
}

function addSplitPaymentParametersToPayfast() {
    // https://developers.payfast.co.za/docs#splitpayments
    // https://stackoverflow.com/questions/52703627/payfast-split-payments
    // https://ideas.woocommerce.com/forums/133476-woocommerce/suggestions/40333552-split-cart-payment-for-payfast-gateway

    // global $post;
    $order_id = absint( get_query_var('order-pay') );
    // echo "order_id: " . $order_id . "<br>";
    // $author_id = $post->post_author;
    // echo "author_id: " . $author_id . "<br>";
    // Loop through products in basket
    $order = wc_get_order( $order_id );
    $items = $order->get_items();
    $total_price = 0;
    foreach ($items as $item) {
        $product_id = $item['product_id'];
        // echo "<br>product_id: " . $product_id;
        $product_owner = get_post_field( 'post_author', $product_id );
        // echo "<br>product_owner: " . $product_owner;
        $listing_title = get_post_field( 'post_title', $product_id );
        // echo "<br>listing_title: " . $listing_title;
        // get listing_id from product
        $listing_id = get_post_meta($product_id, '_homey_listing_id', true);
        // $listing_id = str_replace("Listing ID ", "", $listing_title);
        // $listing_id = $product_id;
        // echo "<br>listing_id: " . $listing_id;
        $listing_author = get_post_field( 'post_author', $listing_id );
        // echo "<br>listing_author: " . $listing_author;
        // echo "<br>listing_author: " . $listing_author;
        $payfast_merchant_id = get_user_meta( $listing_author, 'homey_payfast_merchant_id', true );
        // echo "<br>payfast_merchant_id: " . $payfast_merchant_id;
    }


    // $vendor_id = get_post_field( 'post_author', $order_id );
    // echo "vendor_id: " . $vendor_id . "<br>";
    // $author_id = get_post_field( 'post_author', $page_id );
    // $author_name = get_the_author_meta( 'display_name', $author_id );
    // $payfast_merchant_id = get_user_meta( $author_id, 'homey_payfast_merchant_id', true );
    if (empty($payfast_merchant_id)) {
		// echo "<br>payfast_merchant_id: " . $payfast_merchant_id;
        $payfast_merchant_id = get_user_meta( $listing_author, 'payfast_merchant_id', true );
        // echo "<br>listing_author: " . $listing_author;
        // echo "<br>listing_id: " . $listing_id;
		// echo "<br>payfast_merchant_id: " . $payfast_merchant_id;
        // $payfast_merchant_id = "10012689";
        $payfast_merchant_id = "10000100";  // Sandbox
        return false;
    }
    // For testing
    // $payfast_merchant_id = "10025870";  // Test sandbox account
    // $payfast_merchant_id = "10023922";  // Test sandbox account
    // $payfast_merchant_id = "10012689";  // Pixi?

    // $data_to_send = '
    //         {
    //             "split_payment": {
    //                 "merchant_id": $payfast_merchant_id,
    //                 "percentage": 90,
    //                 "min": 100,
    //                 "max": 100000
    //             }
    //         }
    //     ';
    // $data_to_send = "{'split_payment': {'merchant_id': " . $payfast_merchant_id . ", 'percentage': 90, 'min': 1, 'max': 100000}}";
    $data_to_send = '{"split_payment": {"merchant_id": ' . $payfast_merchant_id . ', "percentage": 89, "min": 1, "max": 100000000}}';
    // echo "<br>data_to_send: " . $data_to_send . "<br>";
    // return json_encode($data_to_send);
    return $data_to_send;
}

if( !function_exists('homey_save_profile') ):

    function homey_save_profile(){
        global $current_user;
        wp_get_current_user();
        $userID  = $current_user->ID;

        $prefix = 'homey_';

        $verify_nonce = $_REQUEST['security'];
        if ( ! wp_verify_nonce( $verify_nonce, 'homey_profile_nonce' ) ) {
            echo json_encode( array( 'success' => false , 'msg' => 'Invalid request' ) );
            die;
        }

        // Update GDPR
        if ( !empty( $_POST['gdpr_agreement'] ) ) {
            $gdpr_agreement = sanitize_text_field( $_POST['gdpr_agreement'] );
            update_user_meta( $userID, 'gdpr_agreement', $gdpr_agreement );
        } else {
            delete_user_meta( $userID, 'gdpr_agreement' );
        }

        if ( !empty( $_POST['firstname'] ) ) {
            $firstname = sanitize_text_field( $_POST['firstname'] );
            update_user_meta( $userID, 'first_name', $firstname );
        } else {
            delete_user_meta( $userID, 'first_name' );
        }

        if ( !empty( $_POST['lastname'] ) ) {
            $lastname = sanitize_text_field( $_POST['lastname'] );
            update_user_meta( $userID, 'last_name', $lastname );
        } else {
            delete_user_meta( $userID, 'last_name' );
        }

        if ( !empty( $_POST['bio'] ) ) {
            $bio = sanitize_text_field( $_POST['bio'] );
            update_user_meta( $userID, 'description', $bio );
        } else {
            delete_user_meta( $userID, 'description' );
        }

        if ( !empty( $_POST['native_language'] ) ) {
            $native_language = sanitize_text_field( $_POST['native_language'] );
            update_user_meta( $userID, $prefix.'native_language', $native_language );
        } else {
            delete_user_meta( $userID, $prefix.'native_language' );
        }

        if ( !empty( $_POST['other_language'] ) ) {
            $other_language = sanitize_text_field( $_POST['other_language'] );
            update_user_meta( $userID, $prefix.'other_language', $other_language );
        } else {
            delete_user_meta( $userID, $prefix.'other_language' );
        }

        if ( !empty( $_POST['payfast_merchant_id'] ) ) {
            $payfast_merchant_id = sanitize_text_field( $_POST['payfast_merchant_id'] );
            update_user_meta( $userID, $prefix.'payfast_merchant_id', $payfast_merchant_id );
        } else {
            delete_user_meta( $userID, $prefix.'payfast_merchant_id' );
        }
        echo "payfast_merchant_id:" .  $payfast_merchant_id . ":payfast_merchant_id";

        if ( !empty( $_POST['street_address'] ) ) {
            $street_address = sanitize_text_field( $_POST['street_address'] );
            update_user_meta( $userID, $prefix.'street_address', $street_address );
        } else {
            delete_user_meta( $userID, $prefix.'street_address' );
        }

        if ( !empty( $_POST['apt_suit'] ) ) {
            $apt_suit = sanitize_text_field( $_POST['apt_suit'] );
            update_user_meta( $userID, $prefix.'apt_suit', $apt_suit );
        } else {
            delete_user_meta( $userID, $prefix.'apt_suit' );
        }

        if ( !empty( $_POST['zipcode'] ) ) {
            $zipcode = sanitize_text_field( $_POST['zipcode'] );
            update_user_meta( $userID, $prefix.'zipcode', $zipcode );
        } else {
            delete_user_meta( $userID, $prefix.'zipcode' );
        }

        if ( !empty( $_POST['country'] ) ) {
            $country = sanitize_text_field( $_POST['country'] );
            update_user_meta( $userID, $prefix.'country', $country );
        } else {
            delete_user_meta( $userID, $prefix.'country' );
        }

        if ( !empty( $_POST['state'] ) ) {
            $state = sanitize_text_field( $_POST['state'] );
            update_user_meta( $userID, $prefix.'state', $state );
        } else {
            delete_user_meta( $userID, $prefix.'state' );
        }

        if ( !empty( $_POST['city'] ) ) {
            $city = sanitize_text_field( $_POST['city'] );
            update_user_meta( $userID, $prefix.'city', $city );
        } else {
            delete_user_meta( $userID, $prefix.'city' );
        }

        if ( !empty( $_POST['neighborhood'] ) ) {
            $neighborhood = sanitize_text_field( $_POST['neighborhood'] );
            update_user_meta( $userID, $prefix.'neighborhood', $neighborhood );
        } else {
            delete_user_meta( $userID, $prefix.'neighborhood' );
        }

        if ( !empty( $_POST['em_contact_name'] ) ) {
            $em_contact_name = sanitize_text_field( $_POST['em_contact_name'] );
            update_user_meta( $userID, $prefix.'em_contact_name', $em_contact_name );
        } else {
            delete_user_meta( $userID, $prefix.'em_contact_name' );
        }

        if ( !empty( $_POST['em_relationship'] ) ) {
            $em_relationship = sanitize_text_field( $_POST['em_relationship'] );
            update_user_meta( $userID, $prefix.'em_relationship', $em_relationship );
        } else {
            delete_user_meta( $userID, $prefix.'em_relationship' );
        }

        if ( !empty( $_POST['em_email'] ) ) {
            $em_email = sanitize_text_field( $_POST['em_email'] );
            update_user_meta( $userID, $prefix.'em_email', $em_email );
        } else {
            delete_user_meta( $userID, $prefix.'em_email' );
        }

        if ( !empty( $_POST['em_phone'] ) ) {
            $em_phone = sanitize_text_field( $_POST['em_phone'] );

            if(houzez_validate_phone_number($em_phone)) {
                update_user_meta( $userID, $prefix.'em_phone', $em_phone );

            } else {
                echo json_encode( array( 'success' => false, 'msg' => esc_html__('Invalid phone number.', 'homey') ) );
                wp_die();
            }
        } else {
            delete_user_meta( $userID, $prefix.'em_phone' );
        }


        // Update facebook
        if ( !empty( $_POST['facebook'] ) ) {
            $facebook = sanitize_text_field( $_POST['facebook'] );
            update_user_meta( $userID, $prefix.'author_facebook', $facebook );
        } else {
            delete_user_meta( $userID, $prefix.'author_facebook' );
        }

        // Update twitter
        if ( !empty( $_POST['twitter'] ) ) {
            $twitter = sanitize_text_field( $_POST['twitter'] );
            update_user_meta( $userID, $prefix.'author_twitter', $twitter );
        } else {
            delete_user_meta( $userID, $prefix.'author_twitter' );
        }

        // Update linkedin
        if ( !empty( $_POST['linkedin'] ) ) {
            $linkedin = sanitize_text_field( $_POST['linkedin'] );
            update_user_meta( $userID, $prefix.'author_linkedin', $linkedin );
        } else {
            delete_user_meta( $userID, $prefix.'author_linkedin' );
        }

        // Update instagram
        if ( !empty( $_POST['instagram'] ) ) {
            $instagram = sanitize_text_field( $_POST['instagram'] );
            update_user_meta( $userID, $prefix.'author_instagram', $instagram );
        } else {
            delete_user_meta( $userID, $prefix.'author_instagram' );
        }

        // Update pinterest
        if ( !empty( $_POST['pinterest'] ) ) {
            $pinterest = sanitize_text_field( $_POST['pinterest'] );
            update_user_meta( $userID, $prefix.'author_pinterest', $pinterest );
        } else {
            delete_user_meta( $userID, $prefix.'author_pinterest' );
        }

        // Update youtube
        if ( !empty( $_POST['youtube'] ) ) {
            $youtube = sanitize_text_field( $_POST['youtube'] );
            update_user_meta( $userID, $prefix.'author_youtube', $youtube );
        } else {
            delete_user_meta( $userID, $prefix.'author_youtube' );
        }

        // Update vimeo
        if ( !empty( $_POST['vimeo'] ) ) {
            $vimeo = sanitize_text_field( $_POST['vimeo'] );
            update_user_meta( $userID, $prefix.'author_vimeo', $vimeo );
        } else {
            delete_user_meta( $userID, $prefix.'author_vimeo' );
        }

        // Update airbnb
        if ( !empty( $_POST['airbnb'] ) ) {
            $airbnb = sanitize_text_field( $_POST['airbnb'] );
            update_user_meta( $userID, $prefix.'author_airbnb', $airbnb );
        } else {
            delete_user_meta( $userID, $prefix.'author_airbnb' );
        }

        // Update trip_advisor
        if ( !empty( $_POST['trip_advisor'] ) ) {
            $trip_advisor = sanitize_text_field( $_POST['trip_advisor'] );
            update_user_meta( $userID, $prefix.'author_trip_advisor', $trip_advisor );
        } else {
            delete_user_meta( $userID, $prefix.'author_trip_advisor' );
        }

        // Update Googleplus
        if ( !empty( $_POST['googleplus'] ) ) {
            $googleplus = sanitize_text_field( $_POST['googleplus'] );
            update_user_meta( $userID, $prefix.'author_googleplus', $googleplus );
        } else {
            delete_user_meta( $userID, $prefix.'author_googleplus' );
        }

        // Update Role
        if ( !empty( $_POST['role'] ) ) {
            $new_role = sanitize_text_field( $_POST['role'] );
            $default_role = get_option('default_role', 1);

            $u = new WP_User( $userID );

            $u->remove_role( $default_role );
            $u->set_role( $new_role );

            update_user_meta($userID, 'social_register_set_role', 1);
        }
        

        // Update email
        if( !empty( $_POST['useremail'] ) ) {
            $useremail = sanitize_email( $_POST['useremail'] );
            $useremail = is_email( $useremail );
            if( !$useremail ) {
                echo json_encode( array( 'success' => false, 'msg' => esc_html__('The Email you entered is not valid. Please try again.', 'homey') ) );
                wp_die();
            } else {
                $email_exists = email_exists( $useremail );
                if( $email_exists ) {
                    if( $email_exists != $userID ) {
                        echo json_encode( array( 'success' => false, 'msg' => esc_html__('This Email is already used by another user. Please try a different one.', 'homey') ) );
                        wp_die();
                    }
                } else {
                    $return = wp_update_user( array ('ID' => $userID, 'user_email' => $useremail, 'display_name' => $display_name ) );
                    if ( is_wp_error( $return ) ) {
                        $error = $return->get_error_message();
                        echo esc_attr( $error );
                        wp_die();
                    }
                }
            }
        }
        wp_update_user( array ('ID' => $userID, 'display_name' => $_POST['display_name'] ) );
        
        echo json_encode( array( 'success' => true, 'msg' => esc_html__('Profile updated', 'homey') ) );
        die();
    }
endif;

// if ( ! function_exists( 'homey_get_price' ) ) {
//     // 20220519: Check when theme updates

//     function homey_get_price() {

//         $homey_prefix = 'homey_';
//         $homey_site_mode = homey_option('homey_site_mode'); // per_hour, per_day, both
//         $day_date_price = get_post_meta( get_the_ID(), $homey_prefix.'day_date_price', true );
//         $night_price = get_post_meta( get_the_ID(), $homey_prefix.'night_price', true );
//         $hour_price = get_post_meta( get_the_ID(), $homey_prefix.'hour_price', true );
//         $booking_type = get_post_meta( get_the_ID(), $homey_prefix.'booking_type', true ); //per_day, per_hour
//         echo "site_mode: " . $homey_site_mode . "<br>";
//         echo "booking_type: " . $booking_type . "<br>";

//         if ($booking_type == 'per_day_date') {
//             $price = $day_date_price;
//         } elseif($homey_site_mode == 'per_day_date' || $homey_site_mode == 'per_day' || $homey_site_mode == 'per_week' || $homey_site_mode == 'per_month') {
//             $price = $night_price;

//         } elseif($homey_site_mode == 'per_hour') {
//             $price = $hour_price;

//         } elseif($homey_site_mode == 'both') {
//             if($booking_type == 'per_day_date') {
//                 $price = $day_date_price;

//             } elseif($booking_type == 'per_day') {
//                 $price = $night_price;

//             } elseif ($booking_type == 'per_hour') {
//                $price = $hour_price;

//             } else {
//                 $price = $night_price;
//             }
//         } else {
//             $price = '';
//         }
//         return $price;
        
//     }
// }

// if ( ! function_exists( 'homey_get_price_by_id' ) ) {

//     function homey_get_price_by_id($listing_id) {

//         $homey_prefix = 'homey_';
//         $homey_site_mode = homey_option('homey_site_mode'); // per_hour, per_day, both
//         $day_date_price = get_post_meta( $listing_id, $homey_prefix.'day_date_price', true );
//         $night_price = get_post_meta( $listing_id, $homey_prefix.'night_price', true );
//         $hour_price = get_post_meta( $listing_id, $homey_prefix.'hour_price', true );
//         $booking_type = get_post_meta( $listing_id, $homey_prefix.'booking_type', true ); //per_day, per_hour
//         echo "site_mode2: " . $homey_site_mode . "<br>";
//         echo "booking_type2: " . $booking_type . "<br>";

//         if ($booking_type == 'per_day_date') {
//             $price = $day_date_price;
//         } elseif($homey_site_mode == 'per_day_date' || $homey_site_mode == 'per_day' || $homey_site_mode == 'per_week' || $homey_site_mode == 'per_month') {
//             $price = $night_price;

//         } elseif($homey_site_mode == 'per_hour') {
//             $price = $hour_price;

//         } elseif($homey_site_mode == 'both') {
//             if($booking_type == 'per_day_date') {
//                 $price = $day_date_price;

//             } elseif($booking_type == 'per_day') {
//                 $price = $night_price;

//             } elseif ($booking_type == 'per_hour') {
//                $price = $hour_price;

//             } else {
//                 $price = $night_price;
//             }
//         } else {
//             $price = '';
//         }
//         return $price;
        
//     }
// }

if( !function_exists('save_listing_as_draft') ) {
function listing_submission_filter($new_listing) {
    global $current_user;

    wp_get_current_user();
    $userID = $current_user->ID;
    $user_email   =   $current_user->user_email;
    $admin_email  =  get_bloginfo('admin_email');
    $totalGuestsPlusAddtionalGuests = 0;

    $listings_admin_approved = homey_option('listings_admin_approved');
    $edit_listings_admin_approved = homey_option('edit_listings_admin_approved');

    // Title
    if( isset( $_POST['listing_title']) ) {
        $new_listing['post_title'] = sanitize_text_field( $_POST['listing_title'] );
    }

    // Description
    if( isset( $_POST['description'] ) ) {
        $new_listing['post_content'] = wp_kses_post( $_POST['description'] );
    }


    if(isset($_POST['post_author_id']) && !empty($_POST['post_author_id']) ) {
        $new_listing['post_author'] = intval($_POST['post_author_id']);
    } else {
        $new_listing['post_author'] = $userID;
    }

    $submission_action = sanitize_text_field($_POST['action']);
    $listing_id = 0;

    $draft_listing_id = isset($_POST['draft_listing_id']) ? $_POST['draft_listing_id'] : '';
    $draft_listing_id = intval($draft_listing_id);
    
    if(!empty($draft_listing_id)) {
        $submission_action = 'update_listing';
    }

    $first_owner_userID = 0;
    if( $submission_action == 'homey_add_listing' || isset($_GET['duplication']) ) {
        $first_owner_userID = $current_user->ID;

        if( ($listings_admin_approved != 0 && !homey_is_admin())) {
            $new_listing['post_status'] = 'pending';
        } else {
            $new_listing['post_status'] = 'publish';
        }

        /*
         * Filter submission arguments before insert into database.
         */
        $new_listing = apply_filters( 'homey_before_submit_listing', $new_listing );

        do_action( 'homey_before_listing_submit', $new_listing);

        $listing_id = wp_insert_post( $new_listing );
    } else if( $submission_action == 'update_listing' ) {
        if(!empty($draft_listing_id)) {
            $new_listing['ID'] = $draft_listing_id;
        } else {
            $new_listing['ID'] = intval( $_POST['listing_id'] );
        }

//            $check_is_approved = get_post_meta( $new_listing['ID'], 'homey_firsttime_is_admin_approved', true );
//            that is removed because was not standard but one clients request.
        $check_is_approved = 1;

        if(($check_is_approved == 0 || $edit_listings_admin_approved != 0) && !homey_is_admin()) {
            $new_listing['post_status'] = 'pending';
        } else {
            $new_listing['post_status'] = 'publish';
            // is in need to be first time approved from admin?
            update_post_meta( $new_listing['ID'], 'homey_firsttime_is_admin_approved', 1 );
        }

        /*
         * Filter submission arguments before update into database.
         */
        $new_listing = apply_filters( 'homey_before_update_listing', $new_listing );

        do_action( 'homey_before_listing_update');

        $listing_id = wp_update_post( $new_listing );

    }

    if( $listing_id > 0 ) {

        $prefix = 'homey_';

        //Custom Fields
        if(class_exists('Homey_Fields_Builder')) {
            $fields_array = Homey_Fields_Builder::get_form_fields();
            if(!empty($fields_array)):
                foreach ( $fields_array as $value ):
                    $field_name = $value->field_id;
                    $field_type = $value->type;

                    if( isset( $_POST[$field_name] ) ) {
                        if($field_type=='textarea') {
                            update_post_meta( $listing_id, 'homey_'.$field_name, $_POST[$field_name] );
                        } else {
                            update_post_meta( $listing_id, 'homey_'.$field_name, sanitize_text_field( $_POST[$field_name] ) );
                        }
                        
                    }

                endforeach; endif;
        }
        
        $listing_total_rating = get_post_meta( $listing_id, 'listing_total_rating', true );
        if( $listing_total_rating === '') {
            update_post_meta($listing_id, 'listing_total_rating', '0');
        }

        // First owner field
        if( $first_owner_userID > 0 ) {
            update_post_meta( $listing_id, $prefix.'first_owner_user_id', sanitize_text_field($first_owner_userID) );
        }

        // Booking type
        if( isset( $_POST['booking_type'] ) ) {
            update_post_meta( $listing_id, $prefix.'booking_type', sanitize_text_field( $_POST['booking_type'] ) );
        }

        // Instance
        if( isset( $_POST['instant_booking'] ) ) { 
            $instance_bk = $_POST['instant_booking'];
            if($instance_bk == 'on') {
                $instance_bk = 1;
            }
            update_post_meta( $listing_id, $prefix.'instant_booking', sanitize_text_field( $instance_bk ) );
        } else {
            update_post_meta( $listing_id, $prefix.'instant_booking', 0 );
        }

        

        // Bedrooms
        if( isset( $_POST['listing_bedrooms'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_bedrooms', sanitize_text_field( $_POST['listing_bedrooms'] ) );
        }

        // Guests
        if( isset( $_POST['guests'] ) ) {
            update_post_meta( $listing_id, $prefix.'guests', sanitize_text_field( $_POST['guests'] ) );

            $totalGuestsPlusAddtionalGuests =  $_POST['guests'];
        }

        // Beds
        if( isset( $_POST['beds'] ) ) {
            update_post_meta( $listing_id, $prefix.'beds', sanitize_text_field( $_POST['beds'] ) );
        }

        // Baths
        if( isset( $_POST['baths'] ) ) {
            update_post_meta( $listing_id, $prefix.'baths', sanitize_text_field( $_POST['baths'] ) );
        }

        // Rooms
        if( isset( $_POST['listing_rooms'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_rooms', sanitize_text_field( $_POST['listing_rooms'] ) );
        }

        // affiliate_booking_link
        if( isset( $_POST['affiliate_booking_link'] ) ) {
            update_post_meta( $listing_id, $prefix.'affiliate_booking_link', sanitize_text_field( $_POST['affiliate_booking_link'] ) );
        }

        // Day Date Price
        if( isset( $_POST['day_date_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'day_date_price', sanitize_text_field( $_POST['day_date_price'] ) );
            update_post_meta( $listing_id, $prefix.'night_price', sanitize_text_field( $_POST['day_date_price'] ) );
        }

        // Day Date Weekend Price
        if( isset( $_POST['day_date_weekends_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'day_date_weekends_price', sanitize_text_field( $_POST['day_date_weekends_price'] ) );
        }

        // Night Price
        if( isset( $_POST['night_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'night_price', sanitize_text_field( $_POST['night_price'] ) );
            update_post_meta( $listing_id, $prefix.'day_date_price', sanitize_text_field( $_POST['night_price'] ) );
        }

        // Weekend Price
        if( isset( $_POST['weekends_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'weekends_price', sanitize_text_field( $_POST['weekends_price'] ) );
        }

        // Hourly Price
        if( isset( $_POST['hour_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'hour_price', sanitize_text_field( $_POST['hour_price'] ) );
        }

        // After Price label
        if( isset( $_POST['price_postfix'] ) ) {
            update_post_meta( $listing_id, $prefix.'price_postfix', sanitize_text_field( $_POST['price_postfix'] ) );
        }

        // Hourly Weekend Price
        if( isset( $_POST['hourly_weekends_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'hourly_weekends_price', sanitize_text_field( $_POST['hourly_weekends_price'] ) );
        }

        // Min book Hours
        if( isset( $_POST['min_book_hours'] ) ) {
            update_post_meta( $listing_id, $prefix.'min_book_hours', sanitize_text_field( $_POST['min_book_hours'] ) );
        }

        // Start Hours
        if( isset( $_POST['start_hour'] ) ) {
            update_post_meta( $listing_id, $prefix.'start_hour', sanitize_text_field( $_POST['start_hour'] ) );
        }

        // End Hours
        if( isset( $_POST['end_hour'] ) ) {
            update_post_meta( $listing_id, $prefix.'end_hour', sanitize_text_field( $_POST['end_hour'] ) );
        }

        if( isset( $_POST['weekends_days'] ) ) {
            update_post_meta( $listing_id, $prefix.'weekends_days', sanitize_text_field( $_POST['weekends_days'] ) );
        }

        // Week( 7 Nights ) Price
        if( isset( $_POST['priceWeek'] ) ) {
            update_post_meta( $listing_id, $prefix.'priceWeek', sanitize_text_field( $_POST['priceWeek'] ) );
        }

        // Monthly ( 30 Nights ) Price
        if( isset( $_POST['priceMonthly'] ) ) {
            update_post_meta( $listing_id, $prefix.'priceMonthly', sanitize_text_field( $_POST['priceMonthly'] ) );
        }

        // Additional Guests price
        if( isset( $_POST['additional_guests_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'additional_guests_price', sanitize_text_field( $_POST['additional_guests_price'] ) );
        }

        // Additional Guests allowed
        if( isset( $_POST['num_additional_guests'] ) ) { 
            update_post_meta( $listing_id, $prefix.'num_additional_guests', sanitize_text_field( $_POST['num_additional_guests'] ) );

            //If additional guests set lets add them to the total count
            $totalGuestsPlusAddtionalGuests += (int) $_POST['num_additional_guests'];
        }

        //Now update the meta data with the total guest count
        update_post_meta( $listing_id, $prefix.'total_guests_plus_additional_guests', $totalGuestsPlusAddtionalGuests );

        // Security Deposit
        if( isset( $_POST['allow_additional_guests'] ) ) {
            update_post_meta( $listing_id, $prefix.'allow_additional_guests', sanitize_text_field( $_POST['allow_additional_guests'] ) );
        }

        // Cleaning fee
        if( isset( $_POST['cleaning_fee'] ) ) {
            update_post_meta( $listing_id, $prefix.'cleaning_fee', sanitize_text_field( $_POST['cleaning_fee'] ) );
        }

        // Cleaning fee
        if( isset( $_POST['cleaning_fee_type'] ) ) {
            update_post_meta( $listing_id, $prefix.'cleaning_fee_type', sanitize_text_field( $_POST['cleaning_fee_type'] ) );
        }

        // City fee
        if( isset( $_POST['city_fee'] ) ) {
            update_post_meta( $listing_id, $prefix.'city_fee', sanitize_text_field( $_POST['city_fee'] ) );
        }

        // City fee
        if( isset( $_POST['city_fee_type'] ) ) {
            update_post_meta( $listing_id, $prefix.'city_fee_type', sanitize_text_field( $_POST['city_fee_type'] ) );
        }

        // securityDeposit
        if( isset( $_POST['security_deposit'] ) ) {
            update_post_meta( $listing_id, $prefix.'security_deposit', sanitize_text_field( $_POST['security_deposit'] ) );
        }

        // securityDeposit
        if( isset( $_POST['tax_rate'] ) ) {
            update_post_meta( $listing_id, $prefix.'tax_rate', sanitize_text_field( $_POST['tax_rate'] ) );
        }

        // Listing size
        if( isset( $_POST['listing_size'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_size', sanitize_text_field( $_POST['listing_size'] ) );
        }

        // Listing size
        if( isset( $_POST['listing_size_unit'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_size_unit', sanitize_text_field( $_POST['listing_size_unit'] ) );
        }

        // Address
        if( isset( $_POST['listing_address'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_address', sanitize_text_field( $_POST['listing_address'] ) );
        }

        //AptSuit
        if( isset( $_POST['aptSuit'] ) ) {
            update_post_meta( $listing_id, $prefix.'aptSuit', sanitize_text_field( $_POST['aptSuit'] ) );
        }


        // Cancellation Policy
        if( isset( $_POST['cancellation_policy'] ) ) {
            update_post_meta( $listing_id, $prefix.'cancellation_policy', sanitize_textarea_field( $_POST['cancellation_policy'] ) );
        }

        // Minimum Stay
        if( isset( $_POST['min_book_days'] ) ) {
            update_post_meta( $listing_id, $prefix.'min_book_days', sanitize_text_field( $_POST['min_book_days'] ) );
        }

        if( isset( $_POST['min_book_weeks'] ) ) {
            update_post_meta( $listing_id, $prefix.'min_book_weeks', sanitize_text_field( $_POST['min_book_weeks'] ) );
        }

        if( isset( $_POST['min_book_months'] ) ) {
            update_post_meta( $listing_id, $prefix.'min_book_months', sanitize_text_field( $_POST['min_book_months'] ) );
        }

        // Maximum Stay
        if( isset( $_POST['max_book_days'] ) ) {
            update_post_meta( $listing_id, $prefix.'max_book_days', sanitize_text_field( $_POST['max_book_days'] ) );
        }
        if( isset( $_POST['max_book_weeks'] ) ) {
            update_post_meta( $listing_id, $prefix.'max_book_weeks', sanitize_text_field( $_POST['max_book_weeks'] ) );
        }
        if( isset( $_POST['max_book_months'] ) ) {
            update_post_meta( $listing_id, $prefix.'max_book_months', sanitize_text_field( $_POST['max_book_months'] ) );
        }

        // Check in After
        if( isset( $_POST['checkin_after'] ) ) {
            update_post_meta( $listing_id, $prefix.'checkin_after', sanitize_text_field( $_POST['checkin_after'] ) );
        }

        // Check Out After
        if( isset( $_POST['checkout_before'] ) ) {
            update_post_meta( $listing_id, $prefix.'checkout_before', sanitize_text_field( $_POST['checkout_before'] ) );
        }

        // Allow Smoke
        if( isset( $_POST['smoke'] ) ) {
            update_post_meta( $listing_id, $prefix.'smoke', sanitize_text_field( $_POST['smoke'] ) );
        }

        // Allow Pets
        if( isset( $_POST['pets'] ) ) {
            update_post_meta( $listing_id, $prefix.'pets', sanitize_text_field( $_POST['pets'] ) );
        }

        // Allow Party
        if( isset( $_POST['party'] ) ) {
            update_post_meta( $listing_id, $prefix.'party', sanitize_text_field( $_POST['party'] ) );
        }

        // Allow Childred
        if( isset( $_POST['children'] ) ) {
            update_post_meta( $listing_id, $prefix.'children', sanitize_text_field( $_POST['children'] ) );
        }

        // Additional Rules
        if( isset( $_POST['additional_rules'] ) ) {
            update_post_meta( $listing_id, $prefix.'additional_rules', sanitize_textarea_field( $_POST['additional_rules'] ) );
        }

        if( isset( $_POST['homey_accomodation'] ) ) {
            $homey_accomodation = $_POST['homey_accomodation'];
            if( ! empty( $homey_accomodation ) ) {
                update_post_meta( $listing_id, $prefix.'accomodation', $homey_accomodation );
            }
        } else {
            update_post_meta( $listing_id, $prefix.'accomodation', '' );
        }

        if( isset( $_POST['homey_services'] ) ) {
            $homey_services = $_POST['homey_services'];
            if( ! empty( $homey_services ) ) {
                update_post_meta( $listing_id, $prefix.'services', $homey_services );
            }
        } else {
            update_post_meta( $listing_id, $prefix.'services', '' );
        }

        if( isset( $_POST['extra_price'] ) ) {
            $extra_price = $_POST['extra_price'];
            if( ! empty( $extra_price ) ) {
                update_post_meta( $listing_id, $prefix.'extra_prices', $extra_price );
            }
        } else {
            update_post_meta( $listing_id, $prefix.'extra_prices', '' );
        }

        // Openning Hours
        if( isset( $_POST['mon_fri_open'] ) ) {
            update_post_meta( $listing_id, $prefix.'mon_fri_open', sanitize_text_field( $_POST['mon_fri_open'] ) );
        }
        if( isset( $_POST['mon_fri_close'] ) ) {
            update_post_meta( $listing_id, $prefix.'mon_fri_close', sanitize_text_field( $_POST['mon_fri_close'] ) );
        }
        if( isset( $_POST['mon_fri_closed'] ) ) {
            update_post_meta( $listing_id, $prefix.'mon_fri_closed', sanitize_text_field( $_POST['mon_fri_closed'] ) );
        } else {
            update_post_meta( $listing_id, $prefix.'mon_fri_closed', 0 );
        }

        if( isset( $_POST['sat_open'] ) ) {
            update_post_meta( $listing_id, $prefix.'sat_open', sanitize_text_field( $_POST['sat_open'] ) );
        }
        if( isset( $_POST['sat_close'] ) ) {
            update_post_meta( $listing_id, $prefix.'sat_close', sanitize_text_field( $_POST['sat_close'] ) );
        }
        if( isset( $_POST['sat_closed'] ) ) {
            update_post_meta( $listing_id, $prefix.'sat_closed', sanitize_text_field( $_POST['sat_closed'] ) );
        } else {
            update_post_meta( $listing_id, $prefix.'sat_closed', 0 );
        }


        if( isset( $_POST['sun_open'] ) ) {
            update_post_meta( $listing_id, $prefix.'sun_open', sanitize_text_field( $_POST['sun_open'] ) );
        }
        if( isset( $_POST['sun_close'] ) ) {
            update_post_meta( $listing_id, $prefix.'sun_close', sanitize_text_field( $_POST['sun_close'] ) );
        }
        if( isset( $_POST['sun_closed'] ) ) {
            update_post_meta( $listing_id, $prefix.'sun_closed', sanitize_text_field( $_POST['sun_closed'] ) );
        } else {
            update_post_meta( $listing_id, $prefix.'sun_closed', 0 );
        }


        // Postal Code
        if( isset( $_POST['zip'] ) ) {
            update_post_meta( $listing_id, $prefix.'zip', sanitize_text_field( $_POST['zip'] ) );
        }

        // Country
        if( isset( $_POST['country'] ) ) {
            $listing_country = sanitize_text_field( $_POST['country'] );
            $country_id = wp_set_object_terms( $listing_id, $listing_country, 'listing_country' );
        }

        // State
        if( isset( $_POST['administrative_area_level_1'] ) ) {
            $listing_state = sanitize_text_field( $_POST['administrative_area_level_1'] );
            $state_id = wp_set_object_terms( $listing_id, $listing_state, 'listing_state' );

            $homey_meta = array();
            $homey_meta['parent_country'] = isset( $_POST['country'] ) ? $_POST['country'] : '';
            if( !empty( $state_id) ) {
                update_option('_homey_listing_state_' . $state_id[0], $homey_meta);
            }
        }

        // City
        if( isset( $_POST['locality'] ) ) {
            $listing_city = sanitize_text_field( $_POST['locality'] );
            $city_id = wp_set_object_terms( $listing_id, $listing_city, 'listing_city' );

            $homey_meta = array();
            $homey_meta['parent_state'] = isset( $_POST['administrative_area_level_1'] ) ? $_POST['administrative_area_level_1'] : '';
            if( !empty( $city_id) ) {
                update_option('_homey_listing_city_' . $city_id[0], $homey_meta);
            }
        }

        // Area
        if( isset( $_POST['neighborhood'] ) ) {
            $listing_area = sanitize_text_field( $_POST['neighborhood'] );
            $area_id = wp_set_object_terms( $listing_id, $listing_area, 'listing_area' );

            $homey_meta = array();
            $homey_meta['parent_city'] = isset( $_POST['locality'] ) ? $_POST['locality'] : '';
            if( !empty( $area_id) ) {
                update_option('_homey_listing_area_' . $area_id[0], $homey_meta);
            }
        }

        // Make featured
        if( isset( $_POST['listing_featured'] ) ) {
            $featured = intval( $_POST['listing_featured'] );
            update_post_meta( $listing_id, 'homey_featured', $featured );
        }


        if( ( isset($_POST['lat']) && !empty($_POST['lat']) ) && (  isset($_POST['lng']) && !empty($_POST['lng'])  ) ) {
            $lat = sanitize_text_field( $_POST['lat'] );
            $lng = sanitize_text_field( $_POST['lng'] );
            $lat_lng = $lat.','.$lng;

            update_post_meta( $listing_id, $prefix.'geolocation_lat', $lat );
            update_post_meta( $listing_id, $prefix.'geolocation_long', $lng );
            update_post_meta( $listing_id, $prefix.'listing_location', $lat_lng );
            update_post_meta( $listing_id, $prefix.'listing_map', '1' );
            update_post_meta( $listing_id, $prefix.'show_map', 1 );
            
            
            if( $submission_action == 'homey_add_listing' ) {
                homey_insert_lat_long($lat, $lng, $listing_id);
            } elseif ( $submission_action == 'update_listing' ) {
                homey_update_lat_long($lat, $lng, $listing_id);
            }



        }

        // Room Type
        if( isset( $_POST['room_type'] ) && ( $_POST['room_type'] != '-1' ) ) {
            wp_set_object_terms( $listing_id, intval( $_POST['room_type'] ), 'room_type' );
        }

        // Listing Type
        if( isset( $_POST['listing_type'] ) && ( $_POST['listing_type'] != '-1' ) ) {
            wp_set_object_terms( $listing_id, intval( $_POST['listing_type'] ), 'listing_type' );
        }

        // Amenities
        if( isset( $_POST['listing_amenity'] ) ) {
            $amenities_array = array();
            foreach( $_POST['listing_amenity'] as $amenity_id ) {
                $amenities_array[] = intval( $amenity_id );
            }
            wp_set_object_terms( $listing_id, $amenities_array, 'listing_amenity' );
        }

        // Facilities
        if( isset( $_POST['listing_facility'] ) ) {
            $facilities_array = array();
            foreach( $_POST['listing_facility'] as $facility_id ) {
                $facilities_array[] = intval( $facility_id );
            }
            wp_set_object_terms( $listing_id, $facilities_array, 'listing_facility' );
        }


        // clean up the old meta information related to images when listing update
        if( $submission_action == "update_listing" ){
            delete_post_meta( $listing_id, 'homey_listing_images' );
            delete_post_meta( $listing_id, '_thumbnail_id' );
        }

        if( isset( $_POST['video_url'] ) ) {
            update_post_meta( $listing_id, $prefix.'video_url', sanitize_text_field( $_POST['video_url'] ) );
        }

        // Listing Images
        if( isset( $_POST['listing_image_ids'] ) ) {
            if (!empty($_POST['listing_image_ids']) && is_array($_POST['listing_image_ids'])) {
                $listing_image_ids = array();
                foreach ($_POST['listing_image_ids'] as $img_id ) {
                    $listing_image_ids[] = intval( $img_id );
                    add_post_meta($listing_id, 'homey_listing_images', $img_id);
                }

                // featured image
                if( isset( $_POST['featured_image_id'] ) ) {
                    $featured_image_id = intval( $_POST['featured_image_id'] );
                    if( in_array( $featured_image_id, $listing_image_ids ) ) {
                        update_post_meta( $listing_id, '_thumbnail_id', $featured_image_id );
                    }
                } elseif ( ! empty ( $listing_image_ids ) ) {
                    update_post_meta( $listing_id, '_thumbnail_id', $listing_image_ids[0] );
                }
            }
        }

        apply_filters('listing_submission_filter_filter', $listing_id);

        if( $submission_action == 'homey_add_listing' ) {
            $post_status_text_user = esc_html__("Your listing status is published", 'homey');
            $post_status_text_admin = esc_html__("This listing status is published", 'homey');

            if( ($listings_admin_approved != 0 && !homey_is_admin())) {
                $post_status_text_user = esc_html__("Your listing status is pending for admin approval", 'homey');
                $post_status_text_admin = esc_html__("This listing status is in need to be approved from you.", 'homey');
            }

            $args = array(
                'listing_title'  =>  get_the_title($listing_id),
                'listing_id'     =>  $listing_id,
                'post_status_user' =>  $post_status_text_user,
                'post_status_admin' =>  $post_status_text_admin,
            );
            /*
             * Send email
             * */
            if( ($listings_admin_approved != 0 && !homey_is_admin())) {
                homey_email_composer( $user_email, 'new_submission_listing', $args );
            }
            
            homey_email_composer( $admin_email, 'admin_new_submission_listing', $args );

            do_action( 'homey_after_listing_submit', $listing_id );

        } else if ( $submission_action == 'update_listing' ) {

            $post_status_text_user = esc_html__("Your listing status is published", 'homey');
            $post_status_text_admin = esc_html__("This listing status is published", 'homey');

            if($edit_listings_admin_approved != 0 && !homey_is_admin()) {
                $post_status_text_user = esc_html__("Your listing status is pending for admin approval", 'homey');
                $post_status_text_admin = esc_html__("This listing status is in need to be approved from you.", 'homey');
            }

            $args = array(
                'listing_title'  =>  get_the_title($listing_id),
                'listing_id'     =>  $listing_id,
                'post_status_user' =>  $post_status_text_user,
                'post_status_admin' =>  $post_status_text_admin,
            );
            /*
             * Send email
             * */
            if($edit_listings_admin_approved != 0 && !homey_is_admin()) {
                homey_email_composer( $user_email, 'update_submission_listing', $args );
            }

            homey_email_composer( $admin_email, 'admin_update_submission_listing', $args );


            do_action( 'houmey_after_listing_update', $listing_id );
        }

        return $listing_id;
    } 

} //listing_submission_filter

add_filter('listing_submission_filter', 'listing_submission_filter');
}


/*-----------------------------------------------------------------------------------*/
// validate Email
/*-----------------------------------------------------------------------------------*/
add_action('wp_ajax_save_as_draft', 'save_listing_as_draft');
if( !function_exists('save_listing_as_draft') ) {
function save_listing_as_draft() {
    
    global $current_user;

    wp_get_current_user();
    $userID = $current_user->ID;

    $new_listing = array(
        'post_type' => 'listing'
    );

    $listings_admin_approved = homey_option('listings_admin_approved');
    $edit_listings_admin_approved = homey_option('edit_listings_admin_approved');

    // Title
    if( isset( $_POST['listing_title']) ) {
        $new_listing['post_title'] = sanitize_text_field( $_POST['listing_title'] );
    }

    // Description
    if( isset( $_POST['description'] ) ) {
        $new_listing['post_content'] = wp_kses_post( $_POST['description'] );
    }

    $new_listing['post_author'] = $userID;

    $listing_id = 0;
    $new_listing['post_status'] = 'draft';

    if( isset($_POST['draft_listing_id']) && !empty( $_POST['draft_listing_id'] ) ) {
        $new_listing['ID'] = $_POST['draft_listing_id'];
        $listing_id = wp_update_post( $new_listing );
    } else {
        $listing_id = wp_insert_post( $new_listing );
    }

    if( $listing_id > 0 ) {

        $prefix = 'homey_';

        //Custom Fields
        if(class_exists('Homey_Fields_Builder')) {
            $fields_array = Homey_Fields_Builder::get_form_fields();
            if(!empty($fields_array)):
                foreach ( $fields_array as $value ):
                    $field_name = $value->field_id;
                    $field_type = $value->type;

                    if( isset( $_POST[$field_name] ) ) {
                        if($field_type=='textarea') {
                            update_post_meta( $listing_id, 'homey_'.$field_name, $_POST[$field_name] );
                        } else {
                            update_post_meta( $listing_id, 'homey_'.$field_name, sanitize_text_field( $_POST[$field_name] ) );
                        }
                        
                    }

                endforeach; endif;
        }
        
        $listing_total_rating = get_post_meta( $listing_id, 'listing_total_rating', true );
        if( $listing_total_rating === '') {
            update_post_meta($listing_id, 'listing_total_rating', '0');
        }
        
        // Booking type
        if( isset( $_POST['booking_type'] ) ) { 
            update_post_meta( $listing_id, $prefix.'booking_type', sanitize_text_field( $_POST['booking_type'] ) );
        }

        // Instance
        if( isset( $_POST['instant_booking'] ) ) { 
            $instance_bk = $_POST['instant_booking'];
            if($instance_bk == 'on') {
                $instance_bk = 1;
            }
            update_post_meta( $listing_id, $prefix.'instant_booking', sanitize_text_field( $instance_bk ) );
        } else {
            update_post_meta( $listing_id, $prefix.'instant_booking', 0 );
        }

        

        // Bedrooms
        if( isset( $_POST['listing_bedrooms'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_bedrooms', sanitize_text_field( $_POST['listing_bedrooms'] ) );
        }

        // Guests
        if( isset( $_POST['guests'] ) ) {
            update_post_meta( $listing_id, $prefix.'guests', sanitize_text_field( $_POST['guests'] ) );
        }

        // Beds
        if( isset( $_POST['beds'] ) ) {
            update_post_meta( $listing_id, $prefix.'beds', sanitize_text_field( $_POST['beds'] ) );
        }

        // Baths
        if( isset( $_POST['baths'] ) ) {
            update_post_meta( $listing_id, $prefix.'baths', sanitize_text_field( $_POST['baths'] ) );
        }

        // Rooms
        if( isset( $_POST['listing_rooms'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_rooms', sanitize_text_field( $_POST['listing_rooms'] ) );
        }

        // Night Price
        if( isset( $_POST['night_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'night_price', sanitize_text_field( $_POST['night_price'] ) );
        }

        // Weekend Price
        if( isset( $_POST['weekends_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'weekends_price', sanitize_text_field( $_POST['weekends_price'] ) );
        }

        // Hourly Price
        if( isset( $_POST['hour_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'hour_price', sanitize_text_field( $_POST['hour_price'] ) );
        }

        // After Price label
        if( isset( $_POST['price_postfix'] ) ) {
            update_post_meta( $listing_id, $prefix.'price_postfix', sanitize_text_field( $_POST['price_postfix'] ) );
        }

        // Hourly Weekend Price
        if( isset( $_POST['hourly_weekends_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'hourly_weekends_price', sanitize_text_field( $_POST['hourly_weekends_price'] ) );
        }

        // Min book Hours
        if( isset( $_POST['min_book_hours'] ) ) {
            update_post_meta( $listing_id, $prefix.'min_book_hours', sanitize_text_field( $_POST['min_book_hours'] ) );
        }

        // Start Hours
        if( isset( $_POST['start_hour'] ) ) {
            update_post_meta( $listing_id, $prefix.'start_hour', sanitize_text_field( $_POST['start_hour'] ) );
        }

        // End Hours
        if( isset( $_POST['end_hour'] ) ) {
            update_post_meta( $listing_id, $prefix.'end_hour', sanitize_text_field( $_POST['end_hour'] ) );
        }

        if( isset( $_POST['weekends_days'] ) ) {
            update_post_meta( $listing_id, $prefix.'weekends_days', sanitize_text_field( $_POST['weekends_days'] ) );
        }

        // Week( 7 Nights ) Price
        if( isset( $_POST['priceWeek'] ) ) {
            update_post_meta( $listing_id, $prefix.'priceWeek', sanitize_text_field( $_POST['priceWeek'] ) );
        }

        // Monthly ( 30 Nights ) Price
        if( isset( $_POST['priceMonthly'] ) ) {
            update_post_meta( $listing_id, $prefix.'priceMonthly', sanitize_text_field( $_POST['priceMonthly'] ) );
        }

        // Additional Guests price
        if( isset( $_POST['additional_guests_price'] ) ) {
            update_post_meta( $listing_id, $prefix.'additional_guests_price', sanitize_text_field( $_POST['additional_guests_price'] ) );
        }

        // Security Deposit
        if( isset( $_POST['allow_additional_guests'] ) ) {
            update_post_meta( $listing_id, $prefix.'allow_additional_guests', sanitize_text_field( $_POST['allow_additional_guests'] ) );
        }

        // Cleaning fee
        if( isset( $_POST['cleaning_fee'] ) ) {
            update_post_meta( $listing_id, $prefix.'cleaning_fee', sanitize_text_field( $_POST['cleaning_fee'] ) );
        }

        // Cleaning fee
        if( isset( $_POST['cleaning_fee_type'] ) ) {
            update_post_meta( $listing_id, $prefix.'cleaning_fee_type', sanitize_text_field( $_POST['cleaning_fee_type'] ) );
        }

        // City fee
        if( isset( $_POST['city_fee'] ) ) {
            update_post_meta( $listing_id, $prefix.'city_fee', sanitize_text_field( $_POST['city_fee'] ) );
        }

        // City fee
        if( isset( $_POST['city_fee_type'] ) ) {
            update_post_meta( $listing_id, $prefix.'city_fee_type', sanitize_text_field( $_POST['city_fee_type'] ) );
        }

        // securityDeposit
        if( isset( $_POST['security_deposit'] ) ) {
            update_post_meta( $listing_id, $prefix.'security_deposit', sanitize_text_field( $_POST['security_deposit'] ) );
        }

        // securityDeposit
        if( isset( $_POST['tax_rate'] ) ) {
            update_post_meta( $listing_id, $prefix.'tax_rate', sanitize_text_field( $_POST['tax_rate'] ) );
        }

        // Listing size
        if( isset( $_POST['listing_size'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_size', sanitize_text_field( $_POST['listing_size'] ) );
        }

        // Listing size
        if( isset( $_POST['listing_size_unit'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_size_unit', sanitize_text_field( $_POST['listing_size_unit'] ) );
        }

        // Address
        if( isset( $_POST['listing_address'] ) ) {
            update_post_meta( $listing_id, $prefix.'listing_address', sanitize_text_field( $_POST['listing_address'] ) );
        }

        //AptSuit
        if( isset( $_POST['aptSuit'] ) ) {
            update_post_meta( $listing_id, $prefix.'aptSuit', sanitize_text_field( $_POST['aptSuit'] ) );
        }


        // Cancellation Policy
        if( isset( $_POST['cancellation_policy'] ) ) {
            update_post_meta( $listing_id, $prefix.'cancellation_policy', sanitize_text_field( $_POST['cancellation_policy'] ) );
        }

        // Minimum Stay
        if( isset( $_POST['min_book_days'] ) ) {
            update_post_meta( $listing_id, $prefix.'min_book_days', sanitize_text_field( $_POST['min_book_days'] ) );
        }

        // Maximum Stay
        if( isset( $_POST['max_book_days'] ) ) {
            update_post_meta( $listing_id, $prefix.'max_book_days', sanitize_text_field( $_POST['max_book_days'] ) );
        }

        // Check in After
        if( isset( $_POST['checkin_after'] ) ) {
            update_post_meta( $listing_id, $prefix.'checkin_after', sanitize_text_field( $_POST['checkin_after'] ) );
        }

        // Check Out After
        if( isset( $_POST['checkout_before'] ) ) {
            update_post_meta( $listing_id, $prefix.'checkout_before', sanitize_text_field( $_POST['checkout_before'] ) );
        }

        // Allow Smoke
        if( isset( $_POST['smoke'] ) ) {
            update_post_meta( $listing_id, $prefix.'smoke', sanitize_text_field( $_POST['smoke'] ) );
        }

        // Allow Pets
        if( isset( $_POST['pets'] ) ) {
            update_post_meta( $listing_id, $prefix.'pets', sanitize_text_field( $_POST['pets'] ) );
        }

        // Allow Party
        if( isset( $_POST['party'] ) ) {
            update_post_meta( $listing_id, $prefix.'party', sanitize_text_field( $_POST['party'] ) );
        }

        // Allow Childred
        if( isset( $_POST['children'] ) ) {
            update_post_meta( $listing_id, $prefix.'children', sanitize_text_field( $_POST['children'] ) );
        }

        // Additional Rules
        if( isset( $_POST['additional_rules'] ) ) {
            update_post_meta( $listing_id, $prefix.'additional_rules', sanitize_text_field( $_POST['additional_rules'] ) );
        }

        if( isset( $_POST['homey_accomodation'] ) ) {
            $homey_accomodation = $_POST['homey_accomodation'];
            if( ! empty( $homey_accomodation ) ) {
                update_post_meta( $listing_id, $prefix.'accomodation', $homey_accomodation );
            }
        } else {
            update_post_meta( $listing_id, $prefix.'accomodation', '' );
        }

        if( isset( $_POST['homey_services'] ) ) {
            $homey_services = $_POST['homey_services'];
            if( ! empty( $homey_services ) ) {
                update_post_meta( $listing_id, $prefix.'services', $homey_services );
            }
        } else {
            update_post_meta( $listing_id, $prefix.'services', '' );
        }

        // Openning Hours
        if( isset( $_POST['mon_fri_open'] ) ) {
            update_post_meta( $listing_id, $prefix.'mon_fri_open', sanitize_text_field( $_POST['mon_fri_open'] ) );
        }
        if( isset( $_POST['mon_fri_close'] ) ) {
            update_post_meta( $listing_id, $prefix.'mon_fri_close', sanitize_text_field( $_POST['mon_fri_close'] ) );
        }
        if( isset( $_POST['mon_fri_closed'] ) ) {
            update_post_meta( $listing_id, $prefix.'mon_fri_closed', sanitize_text_field( $_POST['mon_fri_closed'] ) );
        } else {
            update_post_meta( $listing_id, $prefix.'mon_fri_closed', 0 );
        }

        if( isset( $_POST['sat_open'] ) ) {
            update_post_meta( $listing_id, $prefix.'sat_open', sanitize_text_field( $_POST['sat_open'] ) );
        }
        if( isset( $_POST['sat_close'] ) ) {
            update_post_meta( $listing_id, $prefix.'sat_close', sanitize_text_field( $_POST['sat_close'] ) );
        }
        if( isset( $_POST['sat_closed'] ) ) {
            update_post_meta( $listing_id, $prefix.'sat_closed', sanitize_text_field( $_POST['sat_closed'] ) );
        } else {
            update_post_meta( $listing_id, $prefix.'sat_closed', 0 );
        }


        if( isset( $_POST['sun_open'] ) ) {
            update_post_meta( $listing_id, $prefix.'sun_open', sanitize_text_field( $_POST['sun_open'] ) );
        }
        if( isset( $_POST['sun_close'] ) ) {
            update_post_meta( $listing_id, $prefix.'sun_close', sanitize_text_field( $_POST['sun_close'] ) );
        }
        if( isset( $_POST['sun_closed'] ) ) {
            update_post_meta( $listing_id, $prefix.'sun_closed', sanitize_text_field( $_POST['sun_closed'] ) );
        } else {
            update_post_meta( $listing_id, $prefix.'sun_closed', 0 );
        }


        // Postal Code
        if( isset( $_POST['zip'] ) ) {
            update_post_meta( $listing_id, $prefix.'zip', sanitize_text_field( $_POST['zip'] ) );
        }

        // Country
        if( isset( $_POST['country'] ) ) {
            $listing_country = sanitize_text_field( $_POST['country'] );
            $country_id = wp_set_object_terms( $listing_id, $listing_country, 'listing_country' );
        }

        // State
        if( isset( $_POST['administrative_area_level_1'] ) ) {
            $listing_state = sanitize_text_field( $_POST['administrative_area_level_1'] );
            $state_id = wp_set_object_terms( $listing_id, $listing_state, 'listing_state' );

            $homey_meta = array();
            $homey_meta['parent_country'] = isset( $_POST['country'] ) ? $_POST['country'] : '';
            if( !empty( $state_id) ) {
                update_option('_homey_listing_state_' . $state_id[0], $homey_meta);
            }
        }

        // City
        if( isset( $_POST['locality'] ) ) {
            $listing_city = sanitize_text_field( $_POST['locality'] );
            $city_id = wp_set_object_terms( $listing_id, $listing_city, 'listing_city' );

            $homey_meta = array();
            $homey_meta['parent_state'] = isset( $_POST['administrative_area_level_1'] ) ? $_POST['administrative_area_level_1'] : '';
            if( !empty( $city_id) ) {
                update_option('_homey_listing_city_' . $city_id[0], $homey_meta);
            }
        }

        // Area
        if( isset( $_POST['neighborhood'] ) ) {
            $listing_area = sanitize_text_field( $_POST['neighborhood'] );
            $area_id = wp_set_object_terms( $listing_id, $listing_area, 'listing_area' );

            $homey_meta = array();
            $homey_meta['parent_city'] = isset( $_POST['locality'] ) ? $_POST['locality'] : '';
            if( !empty( $area_id) ) {
                update_option('_homey_listing_area_' . $area_id[0], $homey_meta);
            }
        }

        // Make featured
        if( isset( $_POST['listing_featured'] ) ) {
            $featured = intval( $_POST['listing_featured'] );
            update_post_meta( $listing_id, 'homey_featured', $featured );
        }


        if( ( isset($_POST['lat']) && !empty($_POST['lat']) ) && (  isset($_POST['lng']) && !empty($_POST['lng'])  ) ) {
            $lat = sanitize_text_field( $_POST['lat'] );
            $lng = sanitize_text_field( $_POST['lng'] );
            $lat_lng = $lat.','.$lng;

            update_post_meta( $listing_id, $prefix.'geolocation_lat', $lat );
            update_post_meta( $listing_id, $prefix.'geolocation_long', $lng );
            update_post_meta( $listing_id, $prefix.'listing_location', $lat_lng );
            update_post_meta( $listing_id, $prefix.'listing_map', '1' );
            
            
            if( $submission_action == 'homey_add_listing' ) {
                homey_insert_lat_long($lat, $lng, $listing_id);
            } elseif ( $submission_action == 'update_listing' ) {
                homey_update_lat_long($lat, $lng, $listing_id);
            }



        }

        // Room Type
        if( isset( $_POST['room_type'] ) && ( $_POST['room_type'] != '-1' ) ) {
            wp_set_object_terms( $listing_id, intval( $_POST['room_type'] ), 'room_type' );
        }

        // Listing Type
        if( isset( $_POST['listing_type'] ) && ( $_POST['listing_type'] != '-1' ) ) {
            wp_set_object_terms( $listing_id, intval( $_POST['listing_type'] ), 'listing_type' );
        }

        // Amenities
        if( isset( $_POST['listing_amenity'] ) ) {
            $amenities_array = array();
            foreach( $_POST['listing_amenity'] as $amenity_id ) {
                $amenities_array[] = intval( $amenity_id );
            }
            wp_set_object_terms( $listing_id, $amenities_array, 'listing_amenity' );
        }

        // Facilities
        if( isset( $_POST['listing_facility'] ) ) {
            $facilities_array = array();
            foreach( $_POST['listing_facility'] as $facility_id ) {
                $facilities_array[] = intval( $facility_id );
            }
            wp_set_object_terms( $listing_id, $facilities_array, 'listing_facility' );
        }


        // clean up the old meta information related to images when listing update
        if( $submission_action == "update_listing" ){
            delete_post_meta( $listing_id, 'homey_listing_images' );
            delete_post_meta( $listing_id, '_thumbnail_id' );
        }

        if( isset( $_POST['video_url'] ) ) {
            update_post_meta( $listing_id, $prefix.'video_url', sanitize_text_field( $_POST['video_url'] ) );
        }

        // Listing Images
        if( isset( $_POST['listing_image_ids'] ) ) {
            if (!empty($_POST['listing_image_ids']) && is_array($_POST['listing_image_ids'])) {
                $listing_image_ids = array();
                foreach ($_POST['listing_image_ids'] as $img_id ) {
                    $listing_image_ids[] = intval( $img_id );
                    add_post_meta($listing_id, 'homey_listing_images', $img_id);
                }

                // featured image
                if( isset( $_POST['featured_image_id'] ) ) {
                    $featured_image_id = intval( $_POST['featured_image_id'] );
                    if( in_array( $featured_image_id, $listing_image_ids ) ) {
                        update_post_meta( $listing_id, '_thumbnail_id', $featured_image_id );
                    }
                } elseif ( ! empty ( $listing_image_ids ) ) {
                    update_post_meta( $listing_id, '_thumbnail_id', $listing_image_ids[0] );
                }
            }
        }
    }

    echo json_encode( array( 'success' => true, 'listing_id' => $listing_id, 'msg' => esc_html__('Successfull', 'homey') ) );
    wp_die();
}
}

add_action('init', 'brave_current_user_id');
function brave_current_user_id() {
    $brave_current_user_id = get_current_user_id();
    // brave_write_log("brave_current_user_id(): " . $brave_current_user_id);
    return $brave_current_user_id;
}

add_action('init', 'brave_write_log');
if (!function_exists('brave_write_log')) {
    function brave_write_log($log) {
        if (true === WP_DEBUG) {
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }

            $user_id = apply_filters( 'determine_current_user', false );
            wp_set_current_user( $user_id );
            $current_user = wp_get_current_user();
            $userID       = $current_user->ID;
            error_log("User check 0: " . $userID);

            global $current_user;
            $current_user = wp_get_current_user();
            $userID       = $current_user->ID;
            error_log("User check 1: " . $userID);

            $userID = get_current_user_id();
            error_log("User check 2: " . $userID);

            $userID = brave_current_user_id();
            error_log("User check 3: " . $userID);
        }
    }
}
?>