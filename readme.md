See parent-theme-modifactions.md.

Mik: (2022-04-07)
Homey code errors fixes:

\wp-content\plugins\homey-login-register\social\google\service\Google_Utils.php line 58

    //$ordinalValue = ord($str{$ret});
	$ordinalValue = ord($str[$ret]);

\wp-content\plugins\homey-login-register\social\Facebook\Http\GraphRawResponse.php line 107

	//preg_match('|HTTP/\d\.\d\s+(\d+)\s+.*|', $rawResponseHeader, $match);
	preg_match('/HTTP\/\d(?:\.\d)?\s+(\d+)\s+/', $rawResponseHeader, $match);

\wp-content\plugins\homey-woo-addon\includes\payment.php line 836

	// $reservation_id = homey_add_instance_booking($listing_id, $check_in_date, $check_out_date, $guests, $renter_message, $extra_options);
	$reservation_id = homey_add_instance_booking($listing_id, $check_in_date, $check_out_date, $guests, $renter_message, $extra_options, $userID);

\wp-content\plugins\homey-woo-addon\includes\payment.php line 850
Commented out to prevent duplicate messages:
	// do_action('homey_create_messages_thread', $renter_message, $reservation_id, $userID);

Payfast Sandbox (https://support.payfast.co.za/portal/en/kb/articles/how-do-i-test-woocommerce-in-sandbox-mode):
Merchant ID: 10004002
Merchant Key: q1cd2rdny4a53
Passphase: payfast